#!/bin/bash

## RESTAURAÇÃO DE CONFIGURAÇÕES
git --version
if [ ! $? ]; then
    sudo apt install -y git
fi

rm -f ./freeradius/certs/*~ ./freeradius/certs/dh ./freeradius/certs/*.csr ./freeradius/certs/*.crt ./freeradius/certs/*.p12 ./freeradius/certs/*.der ./freeradius/certs/*.pem ./freeradius/certs/*.key ./freeradius/certs/index.txt* ./freeradius/certs/serial* ./freeradius/certs/*\.0 ./freeradius/certs/*\.1 ./freeradius/certs/ca-crl.pem ./freeradius/certs/ca.crl ./freeradius/certs/passwords.mk
git restore Dockerfile apt.conf docker-compose.yml docker-entrypoint.sh smb.conf resolv.conf krb5.conf freeradius/proxy.conf freeradius/mods-available/ldap freeradius/mods-available/eap freeradius/certs/xpextensions freeradius/certs/server.cnf freeradius/certs/ca.cnf
docker-compose down