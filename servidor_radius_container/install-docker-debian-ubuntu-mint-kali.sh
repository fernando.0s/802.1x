#!/bin/bash

if [ "$(whoami)" = "root" ]; then
    clear; echo
    echo -e "ATENÇÃO: este script não deve ser executado como root."
    echo
    echo -e "Se você está usando Debian e supondo que nele há uma conta de usuário"
    echo -e "cujo login é \e[1m\e[41mseu_usuario\e[0m, então primeiro execute numa só linha a"
    echo -e "instalação do sudo e a inclusão de seu_usuario no grupo sudo com o seguinte comando:"
    echo
    echo -e "\e[1m\e[44mapt update && apt dist-upgrade -y && apt install -y sudo && gpasswd -a \e[1m\e[41mseu_usuario\e[0m\e[1m\e[44m sudo\e[0m"
    echo
    echo -e "onde \e[1m\e[41mseu_usuario\e[0m será subustituído pelo login da sua conta de usuário."
    echo
    echo -e "Após isso, logue-se como \e[1m\e[41mseu_usuario\e[0m e execute este script de instalação do Docker."
    exit 1
fi

#------------------------------------------------
# Instalando o curl e outras dependencias via APT
#------------------------------------------------
echo
echo -e "\e[1m\e[44mInstalando o curl e outras dependencias via APT:\e[0m"
sudo apt-get update && sudo apt-get install -y apt-transport-https ca-certificates curl gnupg software-properties-common lsb-release

#------------------------------------------------
# docker-ce
#------------------------------------------------
echo
echo -e "\e[1m\e[44mInstalando o docker mais recente:\e[0m"
sudo rm -f /usr/share/keyrings/docker-archive-keyring.gpg

SO=$(lsb_release -ds | tr '[:upper:]' '[:lower:]' | cut -d' ' -f1)
VERSION=$(lsb_release -cs)
MINT=$(. /etc/os-release; echo "$ID_LIKE")
MINT_VERSION_UBUNTU=$(. /etc/os-release; echo "$UBUNTU_CODENAME")
KALI=$(. /etc/os-release; echo "$ID_LIKE")
KALI_VERSION=$(. /etc/os-release; echo "$VERSION" | cut -d. -f1)

function proxy_test {
    if [[ $PROXY_SERVER ]]; then
        while true; do
            host=$(echo $PROXY_SERVER | cut -d: -f1)
            port=$(echo $PROXY_SERVER | cut -d: -f2)
            status=$(timeout 1 bash -c '</dev/tcp/'$host'/'$port' && echo $? || echo $?') 2> /dev/null
            if [ $status == 0 ]; then
                break
            else
                error "Verifique a configuração do PROXY_SERVER no arquivo env. "
            fi
        done
    fi
}

if [ $SO == "debian" ] || [ $SO == "ubuntu" ] ; then
    echo -e "\e[1m\e[44mInstalando em ${SO} ${VERSION}\e[0m"
    if [[ $PROXY_SERVER ]]; then
        proxy_test
        curl -x http://$PROXY_SERVER -fsSL https://download.docker.com/linux/${SO}/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    else
        curl -fsSL https://download.docker.com/linux/${SO}/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    fi
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/${SO} ${VERSION} stable" > docker.list
    sudo mv -f docker.list  /etc/apt/sources.list.d/
elif [ $SO == "ubuntu" ]; then
    echo -e "\e[1m\e[44mInstalando em ${SO} ${VERSION}\e[0m"
    if [[ $PROXY_SERVER ]]; then
        proxy_test
        curl -x http://$PROXY_SERVER -fsSL https://download.docker.com/linux/${SO}/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    else
        curl -fsSL https://download.docker.com/linux/${SO}/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    fi
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable"
elif [ $MINT == "ubuntu" ]; then
    echo -e "\e[1m\e[44mInstalando em Linux Mint\e[0m"
    if [[ $PROXY_SERVER ]]; then
        proxy_test
        curl -x http://$PROXY_SERVER -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    else
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    fi
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu ${MINT_VERSION_UBUNTU} stable" > docker.list
    sudo mv -f docker.list  /etc/apt/sources.list.d/
elif [ $KALI == "debian" ] && [ $KALI_VERSION == "2021" ]; then
    echo -e "\e[1m\e[44mInstalando em Kali Linux 2021\e[0m"
    if [[ $PROXY_SERVER ]]; then
        proxy_test
        curl -x http://$PROXY_SERVER -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    else
        curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    fi
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian buster stable" > docker.list
    sudo mv -f docker.list  /etc/apt/sources.list.d/
fi

sudo apt-get update && sudo apt-get install -y docker-ce docker-ce-cli containerd.io
docker_installed="$?"

if [ ! "$(cat /etc/group | grep docker)" ]; then

    echo
    echo -e "\e[1m\e[41mErro na instalação do docker.\e[0m"
    echo -e "\e[1m\e[41mRevise suas configurações de rede e tente novamente.\e[0m"
    exit 1
fi

if [ ! "$(cat /etc/group | grep docker | grep $USER)" ]; then

    # Adicionando o usuário corrente (que está executando o script na máquina)
    # ao docker para que ele mesmo manipule imagens e containers do Docker sem usr o root
    sudo usermod -aG docker $USER
    just_added="yes"
fi

if [ "$docker_installed" = "0" ] ;then

    echo
    echo -e "\e[1m\e[44mdocker instalado com sucesso:\e[0m"
    docker -v

    echo '{'                                 >  daemon.json
    echo '  "bip": "172.17.0.1/16",'         >> daemon.json
    echo '  "default-address-pools": ['      >> daemon.json
    echo '    {'                             >> daemon.json
    echo '      "base": "172.21.0.0/16",'    >> daemon.json
    echo '      "size": 24'                  >> daemon.json
    echo '    }'                             >> daemon.json
    echo '  ]'                               >> daemon.json
    echo '}'                                 >> daemon.json
    sudo mv -f daemon.json /etc/docker/

    echo
    echo -e "\e[1m\e[44mO docker foi configurado para criar networks /24 dentro de 172.21.0.0/16 ;)\e[0m"
fi

if [ "$PROXY_SERVER" ]; then
    proxy_test
    echo "[Service]"                                             >  http-proxy.conf
    echo "Environment=\"HTTP_PROXY=http://$PROXY_SERVER/\""      >> http-proxy.conf
    echo "Environment=\"HTTPS_PROXY=http://$PROXY_SERVER/\""     >> http-proxy.conf
    echo "Environment=\"NO_PROXY=*.,10.*\""                      >> http-proxy.conf

    if [ ! -d /etc/systemd/system/docker.service.d/ ]; then
        sudo mkdir /etc/systemd/system/docker.service.d/
    fi
    sudo mv -f http-proxy.conf /etc/systemd/system/docker.service.d/
else
    sudo rm -rf /etc/systemd/system/docker.service.d/
fi

sudo systemctl daemon-reload
sudo systemctl restart docker.service

if [ "$just_added" ]; then

    echo
    echo -e "\e[1m\e[44mCom o seu usuário você poderá dar comandos do Docker sem precisar invocar o root.\e[0m"
    echo

    # não precisa, pois o comando "newgrp docker" carrega o grupo sem precisar de nova sessão.
    #sudo su $USER
    newgrp docker
fi
