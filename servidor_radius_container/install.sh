#!/bin/bash

set +e
set -o noglob

RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
ORANGE=$(tput setaf 202)
BOLD=$(tput bold)
UNDERLINE=$(tput sgr 0 1)
RESET=$(tput sgr0)

underline() { printf "${UNDERLINE}${BOLD}%s${RESET}\n" "$@"
}
h1() { printf "\n${UNDERLINE}${BOLD}${YELLOW}%s${RESET}\n" "$@"
}
h2() { printf "\n${UNDERLINE}${BOLD}${MAGENTA}%s${RESET}\n" "$@"
}
debug() { printf "${WHITE}%s${RESET}\n" "$@"
}
info() { printf "${BLUE}➜ %s${RESET}\n" "$@"
}
success() { printf "${GREEN}✔ %s${RESET}\n" "$@"
}
error() { printf "${RED}✖ %s${RESET}\n" "$@"
}
warn() { printf "${ORANGE}➜ %s${RESET}\n" "$@"
}
bold() { printf "${BOLD}%s${RESET}\n" "$@"
}
note() { printf "\n${UNDERLINE}${BOLD}${BLUE}Nota:${RESET} ${BLUE}%s${RESET}\n" "$@"
}

set -e
set +o noglob

function start_container {
  if [ ! -e "docker-compose.yml" ]
  then
    error "Não é possível encontrar 'docker-compose.yml'"
    exit 1
  fi
  success "Criando images e iniciando serviços ..."
  docker-compose -f docker-compose.yml up --build -d
}
function configure_files {
    while true; do
        read -p "Terminou de configura-los? S|N " r
        case $r in
            [Sssim]* ) break;;
            * ) warn "Por favor, configure-os agora!";;
        esac
    done
}
function configure_krb5_resolv_smb_compose {
    info "Configurando krb5 resolv smd compose"
    sed -i 's/AD.LAB.INTRANET/'${SAMBA_REALM}'/' krb5.conf
    sed -i 's/192.168.0.100/'${RESOLV_DNS_1}'/' resolv.conf
    sed -i 's/192.168.0.101/'${RESOLV_DNS_2}'/' resolv.conf
    sed -i 's/ad.lab.intranet/'${SAMBA_FQDN}'/' resolv.conf
    sed -i 's/lab.intranet/'${RESOLV_DOMAIN}'/' resolv.conf
    sed -i 's/LAB/'${DOMAIN_UPPER}'/' smb.conf
    sed -i 's/realm = AD.CINDACTA3.INTRANET/realm = '${SAMBA_REALM}'/' smb.conf
    sed -i 's/RADIUS_TESTE/'${HOSTNAME_CONTAINER}'/' docker-compose.yml
}
function configure_env {
    while true; do
        if [ "${SAMBA_FQDN}" == "ad.lab.intranet" ] || [ "${DNS_1}" == "192.168.0.100" ]; then
            error "Por favor, configure o arquivo env, ele não foi modificado!"

            read -p "Terminou a configuração? S|N " r
            case $r in
                [Sssim]* ) info "Checando configuração...";;
                * ) error "Por favor, configure-o agora!";;
            esac
        else
            break
        fi
    done
}
function export_env {
    info "Exportando variáveis de ambiente"
    for i in $(cat env); do 
        export $i
    done
}
function install_docker {
    info "Instalação do DOCKER mais recente."
    info "Verificando S.O. para instalação do DOCKER"
    so1=$(lsb_release -ds | tr '[:upper:]' '[:lower:]' | cut -d' ' -f1)
    so2=$(lsb_release -ds | tr '[:upper:]' '[:lower:]' | cut -d' ' -f2)
    if [ $so1 == "debian" ] || [ $so1 == "ubuntu" ] || [ $so2 == "mint" ] || [ $so1 == "kali" ]; then
        sed -i 's/intranet/'$NO_PROXY'/' install-docker-debian-ubuntu-mint-kali.sh
        ./install-docker-debian-ubuntu-mint-kali.sh
    else
        error "Esse S.O. não está no script de instalação do DOCKER."
    fi
}
function install_docker_compose {
    if [ -f /usr/local/bin/docker-compose ] ; then
        sudo mv -f /usr/local/bin/docker-compose /usr/local/bin/docker-compose.old
    fi

    if [[ $PROXY_SERVER ]]; then
        while true; do
            host=$(echo $PROXY_SERVER | cut -d: -f1)
            port=$(echo $PROXY_SERVER | cut -d: -f2)
            status=$(timeout 1 bash -c '</dev/tcp/'$host'/'$port' && echo $? || echo $?') 2> /dev/null
            if [ $status == 0 ]; then
                sudo curl -x http://$PROXY_SERVER -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
                sudo chmod 755 /usr/local/bin/docker-compose
                break
            else
                error "Verifique a configuração do PROXY_SERVER no arquivo env. "
            fi
        done
    else
        sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
        docker_compose_installed="$?"
        sudo chmod 755 /usr/local/bin/docker-compose
    fi
}
function install_docker_docker_compose {
    install_docker
    install_docker_compose
}
function check_access_internet {
    while true; do
        read -p "Seu acesso à Internet é via Proxy? S|N " r
        case $r in
            [Sssim]* ) export_env; break;;
            [Nnnao]* ) break;;
            * ) error "Por favor responda S ou N.";;
        esac
    done
}
function install_docker_or_compose {
    while true; do
        read -p "Deseja instalar|atualizar o (D)ocker|(C)ompose|(A)mbos|(N)ão? D|C|A|N " r
        case $r in
            [Dd]* ) check_access_internet; install_docker; break;;
            [Cc]* ) check_access_internet; install_docker_compose; break;;
            [Aa]* ) check_access_internet; install_docker_docker_compose; break;;
            [Nn]* ) break;;
            * ) error "Por favor responda (D)ocker ou (C)ompose ou (A)mbos ou (N)ão".;;
        esac
    done
}
function check_docker_docker_compose {
    info "Checando DOCKER e DOCKER-COMPOSE"

    if [ -e /usr/bin/docker ]; then
        docker --version
        success "DOCKER já instalado!"
    else
		error "Foi detectado que o S.O. não possui DOCKER"
        info "Iniciando instalação!"
    fi

    if [ -e /usr/local/bin/docker-compose ]; then
        docker-compose --version
        success "DOCKER-COMPOSE já instalado!"
    else
    	error "Foi detectado que o S.O. não possui DOCKER-COMPOSE."
        info "Iniciando instalação!"
	fi
    install_docker_or_compose
}
function configure_repository {
    export_env
    if [ "$REPOSITORY_PACKAGES_SERVER" == "repository.lab.intranet:8000" ];then
        error "Seu repositório de pacotes no arquivo env é repository.lab.intranet:8000!"
        while true; do
            read -p "Deseja utilizar o padrão do S.O. do container? S|N " r
            case $r in
                [Sssim]* ) echo "" > apt.conf; break;;
                [Nnnao]* ) echo ; echo "Configure REPOSITORY_PACKAGES_SERVER no env e tente novamente!"; exit 1;;
                * ) error "Por favor responda S ou N.";;
            esac
        done
    else
        echo -e 'Acquire {\n  HTTP::proxy "http://host:port/";\n  HTTPS::proxy "http://host:port/";\n  FTP::proxy "http://host:port/";\n}' | tee apt.conf 1> /dev/null;
        while true; do
            host=$(echo $REPOSITORY_PACKAGES_SERVER | cut -d: -f1)
            port=$(echo $REPOSITORY_PACKAGES_SERVER | cut -d: -f2)
            status=$(timeout 1 bash -c '</dev/tcp/'$host'/'$port' && echo $? || echo $?') 2> /dev/null
            if [ $status == 0 ]; then
                sed -i 's/host:port/'$REPOSITORY_PACKAGES_SERVER'/' apt.conf
                break
            else
                error "Verifique a configuração do repositorio no arquivo env. "
            fi
        done
    fi
}
function container_test {    
    info "Gerando certificados"
    make -C freeradius/certs/ ca
    make -C freeradius/certs/ server
    success "Certificados gerados"
    
    info "Desativando módulo LDAP"
    sed -i 's/RUN ln -s/#RUN ln -s/' Dockerfile

    info "Ligando o modo DEBUG"
    sed -i 's/#    command/    command/' docker-compose.yml

    info "Desativando ingresso no domínio"
    sed -i 's/echo/#echo/' docker-entrypoint.sh
    sed -i 's/ntlm_auth/#ntlm_auth/' docker-entrypoint.sh
    sed -i 's/net ads join -U/#net ads join -U/' docker-entrypoint.sh
    sed -i 's/\/etc/#\/etc/' docker-entrypoint.sh

    start_container
    success "Container iniciado!"
    note "1. Para testar, execute: "
    warn "radtest -t mschap test_user Abc1234. 127.0.0.1 1812 Abc1234."
    note "2. Ao término dos testes execute o script:"
    warn "./remove_configurations.sh"
}
function container_redundant {
    info "Criando um container redundante"
    export_env
    configure_env
    configure_krb5_resolv_smb_compose

    if [ -e ./freeradius/certs/ca.pem ] && [ -e ./freeradius/certs/server.pem ]; then
        start_container
    else
        error "Os certificados do servidor de produção não foram encontrados!"
        note "Você precisa copiar /etc/freeradius do container de produção para a pasta freeradius."
        init_options
    fi
}
function container_new_org {
    info "Criando um container para uma nova organização"
    export_env
    configure_env
    configure_krb5_resolv_smb_compose

    info "Limpando /freeradius/certs/ para geração de novos certificados"
    rm -f ./freeradius/certs/*~ ./freeradius/certs/dh ./freeradius/certs/*.csr ./freeradius/certs/*.crt ./freeradius/certs/*.p12 ./freeradius/certs/*.der ./freeradius/certs/*.pem ./freeradius/certs/*.key ./freeradius/certs/index.txt* ./freeradius/certs/serial* ./freeradius/certs/*\.0 ./freeradius/certs/*\.1 ./freeradius/certs/ca-crl.pem ./freeradius/certs/ca.crl ./freeradius/certs/passwords.mk
    
    info "Preparando arquivos para gerar certificados"
    if [ -e ./freeradius/certs/ca.cnf ] && [ -e ./freeradius/certs/server.cnf ] && [ -e ./freeradius/certs/xpextensions ]; then
        sed -i 's/Abc1234./'$CERT_CA_PASS'/' ./freeradius/certs/ca.cnf
        sed -i 's/lab_ca/'$DOMAIN_LOWER'_ca/' ./freeradius/certs/ca.cnf
        sed -i 's/LAB/'$DOMAIN_UPPER'/' ./freeradius/certs/ca.cnf
        sed -i 's/lab.intranet/'$RESOLV_DOMAIN'/' ./freeradius/certs/ca.cnf
        sed -i 's/Abc1234./'$CERT_SERVER_PASS'/' ./freeradius/certs/server.cnf
        sed -i 's/lab_ca/'$DOMAIN_LOWER'_ca/' ./freeradius/certs/server.cnf
        sed -i 's/LAB/'$DOMAIN_UPPER'/' ./freeradius/certs/server.cnf
        sed -i 's/lab.intranet/'$RESOLV_DOMAIN'/' ./freeradius/certs/server.cnf
        sed -i 's/lab.intranet/'$RESOLV_DOMAIN'/' ./freeradius/certs/xpextensions
        sed -i 's/lab_ca.crl/'$DOMAIN_LOWER'_ca.crl/' ./freeradius/certs/xpextensions
    fi
    
    info "Gerando certificados"
    make -C freeradius/certs/ ca
    make -C freeradius/certs/ server
    success "Certificados gerados"

    info "Configurando variáveis de ambiente no arquivos de configuração"
    if [ -e ./freeradius/mods-available/ldap ] && [ -e ./freeradius/mods-available/eap ] && [ -e ./freeradius/proxy.conf ]; then
        sed -i 's/ldap_user/'$SAMBA_USER'/' ./freeradius/mods-available/ldap
        sed -i 's/Abc1234./'$SAMBA_PASS'/' ./freeradius/mods-available/ldap
        sed -i 's/ad.lab.intranet/'$SAMBA_FQDN'/' ./freeradius/mods-available/ldap
        sed -i 's/dc=ad,dc=lab,dc=intranet/'$SAMBA_BASEDN'/' ./freeradius/mods-available/ldap
        sed -i 's/Abc1234./'$CERT_SERVER_PASS'/' ./freeradius/mods-available/eap
        sed -i 's/LAB/'$DOMAIN_UPPER'/' ./freeradius/proxy.conf
    fi

    while true; do
        read -p "Você configurou os arquivos clients.conf e users do diretorio freeradius? S|N? " r
        case $r in
            [Sssim]* ) info "OK, vamos iniciar o container!"; break;;
            [Nnnao]* ) warn "Por favor configure-os agora!"; configure_files; break;;
            * ) error "Por favor responda sim ou nao.";;
        esac
    done

    info "Iniciando container para uma nova organização"
    start_container
    
    note "1. TESTE DO NTLM_AUTH:"
    warn "ntlm_auth --request-nt-key --domain='${SAMBA_REALM}' --username='${SAMBA_USER}' --password='${SAMBA_PASS}'"
    note "2. TESTE DO RADIUS: "
    warn "radtest -t mschap test_user Abc1234. 127.0.0.1 1812 Abc1234."
    note "3. Caso queira remover as configurações e o container, execute:"
    warn "./remove_configurations.sh"
}
function init_options {
    while true; do
        read -p "Deseja um container de (T)este ou (R)edundante ou para uma nova (O)rganização? T|R|O? " r
        case $r in
            [Tt]* ) container_test; break;;
            [Rr]* ) container_redundant; break;;
            [Oo]* ) container_new_org; break;;
            * ) error "Por favor responda T ou R ou O.";;
        esac
    done
}
check_docker_docker_compose
configure_repository
init_options

bold "REMOVENDO CONTAINERS E IMAGENS DESNECESSÁRIAS"
docker ps -a | grep "Exited" | awk '{print $1}' | xargs -r docker rm
docker ps -a | grep "Created" | awk '{print $1}' | xargs -r docker rm
docker images | grep "<none>" | awk '{print $3}' | xargs -r docker rmi