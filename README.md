# Autenticação 802.1x em redes com fio integrado com Samba AD/DC

Neste cenário de controle de acesso a rede, teremos 01 usuário cadastrado no SERVIDOR DE AUTENTICAÇÃO RADIUS para utilização nos suplicantes Linux, Windows e Impressoras. Caso o usuário possua poderes administrativos do suplicante, deverá utilizar seu próprio usuário do Active Directory. Também será possível realizar login em switches e roteadores com o usuário do Active Directory.

**<div align="left">Cenário</div>**
![](./imagens/8021x-1.png)

**<div align="left">Antes e Depois da Autenticação</div>**
![](./imagens/8021x-2.png)

**<div align="left">Autenticação utilizando certificados para garantir ao suplicante que servidor é legítimo</div>**
1. Servidor envia seu certificado ao suplicante
2. Suplicante possui o CA do servidor e verifica se o certificado recebido foi assinado pelo CA do servidor
3. Suplicante passa a confiar no servidor e envia o usuário e senha
4. Servidor verifica se o usuário e senha estão corretos

![](./imagens/8021x-3.png)
#
#
# Manuais de Configuração
### [1.Servidor RADIUS](./manuais/1.Configuracao_servidor_RADIUS.md)
#
### [2.Autenticador CISCO](./manuais/2.Configuracao_autenticador_CISCO.md)
#
### [3.Serviço de monitoramento](./manuais/3.Configuracao_servico_de_monitoramento.md)
#
### [4.Suplicante LINUX](./manuais/4.Configuracao_suplicante_LINUX.md)
#
### [5.Suplicante WINDOWS](./manuais/5.Configuracao_suplicante_WINDOWS.md)
#
### [6.Suplicante impressora HP](./manuais/6.Configuracao_suplicante_impressora_HP.md)
#
### [7.Suplicante impressora KYOCERA](./manuais/7.Configuracao_suplicante_impressora_KYOCERA.md)
#