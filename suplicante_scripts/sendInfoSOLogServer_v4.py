#!/usr/bin/python3

""" 
## EXEMPLO DE USO
## Para testes, execute a linha abaixo no terminal alterando o IP e PORTA para o
## IP e PORTA que o Servidor de Logs está escutando.

python3 sendInfoSOLogServer.py 127.0.0.1 5143

## EXECUÇÃO DO SCRIPT DURANTE O LOGIN DE HOSTS CLIENTES
## 1. Copie o script python sendInfoSOLogServer.py para /opt/logs-server

mkdir -p /opt/logs-server
cp sendInfoSOLogServer.py /opt/logs-server/

## 2. Crie um script shell em /etc/profile.d/, com as linhas abaixo, para 
##    executar o script python durante o login de qualquer usuário.

nano /etc/profile.d/sendInfoSOLogServer.sh

    #!/bin/sh
    /usr/bin/python3 '/opt/logs-server/sendInfoSOLogServer.py' IP_LOG_SERVER PORT_LOG_SERVER

chmod +x /etc/profile.d/sendInfoSOLogServer.sh

"""

import os
import platform
import socket
import sys
import subprocess


try:
    import json
except ImportError:
    import simplejson as json

RAM_Types = {
    0 : "DDR4",
    1 : "Other",
    2 : "DRAM",
    3 : "Synchronous DRAM",
    4 : "Cache DRAM",
    5 : "EDO",
    6 : "EDRAM",
    7 : "VRAM",
    8 : "SRAM",
    9 : "RAM",
    10 : "ROM",
    11 : "Flash",
    12 : "EEPROM",
    13 : "FEPROM",
    14 : "EPROM",
    15 : "CDRAM",
    16 : "3DRAM",
    17 : "SDRAM",
    18 : "SGRAM",
    19 : "RDRAM",
    20 : "DDR",
    21 : "DDR2",
    22 : "DDR2 FB-DIMM",
    24 : "DDR3",
    25 : "FBD2",
}

class SystemInfo(object):
    """ System Information"""
    def __init__(self):
        self.title = "System Management"

    def platformName(self):
        return platform.system()

    def systemSpec(self):
        """ Collect Hardware details for Linux, Windows and Mac """

        data = {}

        if os_info.platformName() == 'Linux':
            Hardware_info = self.getUnixSystemSpec(data)
        elif os_info.platformName() == 'Windows':
            Hardware_info = self.getWindowsSystemSpec(data)
        elif os_info.platformName() == 'Darwin':
            Hardware_info = self.getMacSystemSpec(data)
        else:
            print("Operating system is not detected.")
            sys.exit(0)

        return Hardware_info

    def getUnixSystemSpec(self, data):
        """ Collect hardware info for Linux """
        try:
            data["Architecture"] = platform.architecture()[0]
        except Exception as error:
            data["Architecture"] = None
        try:
            data["Host Name"] = subprocess.check_output("hostname", stderr=open(os.devnull, 'w'), shell=True).decode('utf-8').strip("\n")
        except Exception as error:
            data["Host Name"] = None
        try:
            data["Ip"] = subprocess.check_output("ip route get 1",stderr=open(os.devnull, 'w'), shell=True).decode('utf-8').split('src ')[1].split(' ')[0]
        except Exception as error:
            data["Ip"] = None
        try:
            data["Manufacturer"] = str(subprocess.check_output('sudo dmidecode -s system-manufacturer', stderr=open(os.devnull, 'w'), shell=True))[2:-3]
        except Exception as error:
            data["Manufacturer"] = None
        try:
            data["Model"] = str(subprocess.check_output('sudo dmidecode -s system-product-name', stderr=open(os.devnull, 'w'), shell=True))[2:-3]
        except Exception as error:
            data["Model"] = None
        try:
            data["OS Version"] = str(subprocess.check_output("cat /etc/*-release | grep VERSION_ID",stderr=open(os.devnull, 'w'), shell=True)).partition('VERSION_ID=')[2].strip('"')[:-4]
        except Exception as error:
            data["OS Version"] = None
        try:
            data["Operating System"] = str(subprocess.check_output("cat /etc/*-release | grep NAME=",stderr=open(os.devnull, 'w'), shell=True)).partition("\\nNAME=")[2].split("\\n")[0].strip('"')
        except Exception as error:
            data["Operating System"] = None
        try:
            data["Serial_Number"] = str(subprocess.check_output('sudo dmidecode -s system-serial-number', stderr=open(os.devnull, 'w'), shell=True))[2:-3]
        except Exception as error:
            data["Serial_Number"] = None
        try:
            data["User"] = os.popen("echo $USER").read().replace("\n","")
        except Exception as error:
            data["User"] = None

        return data

    def getWindowsSystemSpec(self, data):
        """ Collect hardware info for Windows """
        try:
            data["Architecture"] = platform.architecture()[0]
        except Exception as error:
            data["Architecture"] = None

        data["Host Name"] = platform.node()

        data["Ip"] = socket.gethostbyname(socket.gethostname())

        try:
            data["Manufacturer"] = str(subprocess.check_output('wmic computersystem get manufacturer', stderr=open(os.devnull, 'w'), shell=True)).split()[1].strip('\\r\\n')
        except Exception as error:
            data["Manufacturer"] = None
        try:
            data["Model"] = subprocess.check_output('wmic computersystem get model', stderr=open(os.devnull, 'w'), shell=True).decode('utf-8').partition('Model')[2].strip(' \r\n')
        except Exception as error:
            data["Model"] = None

        data["OS Version"] = platform.version()

        try:
            data["Operating System"] = subprocess.check_output('systeminfo | findstr /B /C:"OS Name"',stderr=open(os.devnull, 'w'), shell=True).decode('utf-8').replace('  ','').partition('OS Name:')[2].strip(' \r\n')
        except Exception as error:
            data["Operating System"] = "{} {}".format(platform.system(), platform.release())
        try:
            data["Serial_Number"] = str(subprocess.check_output('wmic bios get serialnumber', stderr=open(os.devnull, 'w'), shell=True)).split()[1].strip('\\r\\n')
        except Exception as error:
            data["Serial_Number"] = None
        try:
            data["User"] = os.popen("echo %USERNAME%").read().replace("\n","")
        except Exception as error:
            data["User"] = None

        return data

    def getMacSystemSpec(self, data):
        """ Collect hardware info for Linux """
        try:
            data["Architecture"] = platform.architecture()[0]
        except Exception as error:
            data["Architecture"] = None
        try:
            data["Host Name"] = subprocess.check_output("hostname", stderr=open(os.devnull, 'w'), shell=True).decode('utf-8').strip('\n')
        except Exception as error:
            data["Host Name"] = None
        try:
            data["Ip"] = subprocess.check_output('route -n get 1 | grep interface | cut -d ":" -f2 | xargs ipconfig getifaddr', stderr=open(os.devnull, 'w'), shell=True).decode('utf-8').strip('\n')
        except Exception as error:
            data["Ip"] = None

        data["Manufacturer"] = "Apple"

        try:
            data["Model"] = str(subprocess.check_output("system_profiler SPHardwareDataType | grep 'Model Name'", stderr=open(os.devnull, 'w'), shell=True)).split(':')[1][1:-3]
        except Exception as error:
            data["Model"] = None
        try:
            data["OS Version"] = subprocess.check_output("sw_vers -productVersion",stderr=open(os.devnull, 'w'), shell=True).decode('utf-8').strip('\n')
        except Exception as error:
            data["OS Version"] = None
        try:
            data["Operating System"] = subprocess.check_output("sw_vers -productName",stderr=open(os.devnull, 'w'), shell=True).decode('utf-8').strip('\n')
        except Exception as error:
            data["Operating System"] = None
        try:
            data["Serial_Number"] = str(subprocess.check_output('system_profiler SPHardwareDataType | grep Serial', stderr=open(os.devnull, 'w'), shell=True)).split(':')[1][1:-3]
        except Exception as error:
            data["Serial_Number"] = None
        try:
            data["User"] = os.popen("echo $USER").read().replace("\n","")
        except Exception as error:
            data["User"] = None

        return data

os_info = SystemInfo()
sysInfo = os_info.systemSpec()

""" Send data for Log Server """
if len(sys.argv) == 3:
    logserver_ip   = sys.argv[1]
    logserver_port = int(sys.argv[2])
else:
    logserver_ip   = "127.0.0.1"
    logserver_port = 5143

#s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # TCP
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
s.connect((logserver_ip, logserver_port))
# s.send(json.dumps(sysInfo)) # Python2
s.send(bytes(json.dumps(sysInfo), 'UTF-8')) # Python3
s.close()

#print(json.dumps(sysInfo,sort_keys=True,indent = 2, separators= (',', ': ')))
