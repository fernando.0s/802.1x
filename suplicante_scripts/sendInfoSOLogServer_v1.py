# !/usr/bin/python3

# Exemplo de uso no WINDOWS:
# python enviarLogJson.py 127.0.0.1 5143 %USERNAME%

# Exemplo de uso no LINUX:
# python enviarLogJson.py 127.0.0.1 5143 $USER

import sys
import socket
import platform

try:
    import json
except ImportError:
    import simplejson as json

logserver_ip   = sys.argv[1]
logserver_port = int(sys.argv[2])

json_message = {}
json_message['user'] = sys.argv[3]
json_message['system'] = platform.system() + " " + platform.uname().release
json_message['architecture'] = platform.architecture()[0]
json_message['machine'] = platform.machine()
json_message['hostname'] = platform.node()
json_message['version'] = platform.version()
json_message['processor'] = platform.processor()


#s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # TCP
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
s.connect((logserver_ip, logserver_port))
# s.send(json.dumps(json_message)) # Python2
s.send(bytes(json.dumps(json_message), 'UTF-8')) # Python3
s.close()

print(json.dumps(json_message,sort_keys=True,indent = 2, separators= (',', ': ')))

