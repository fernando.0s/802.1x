**<div align="left">Requisitos mínimos do servidor Radius:</div>**

* 768MB RAM (2GB RAM recomendado)
* Intel DUal Core de 3.0GHz
* 10GB Disco Rígido

## 1. Preparação do Controlador de Domínio Samba AD/DC para integração com SERVIDOR DE AUTENTICAÇÃO

Habilite os protocolos `MS-CHAPv2` e `NT/LMv2`

```bash
nano /usr/local/samba/etc/smb.conf
```
```bash
[global]

ntlm auth = mschapv2-and-ntlmv2-only
```

Reinicie o Samba e teste as configurações.

```bash
/etc/init.d/samba-ad-dc restart
samba-tool testparm
smbcontrol all reload-config
smbtree
```

Crie grupos e usuários e adicione cada usuário em um grupo diferente.

```bash
# Criação de grupos
samba-tool group add setor-a
samba-tool group add setor-b
samba-tool group add visitantes
# Criação de usuário
samba-tool user create usuario1 Abc1234. --given-name="USUARIO" --surname="1" --department="SETOR-A" --mail=usuario1@ad.lab.intranet
samba-tool user create usuario2 Abc1234. --given-name="USUARIO" --surname="2" --department="SETOR-B" --mail=usuario2@ad.lab.intranet
samba-tool user create visitante1 Abc1234. --given-name="VISITANTE" --surname="1" --department="VISITANTES" --mail=visitante1@ad.lab.intranet
# Adição de usuário no grupo
samba-tool group addmembers "setor-a" usuario1
samba-tool group addmembers "setor-b" usuario2
samba-tool group addmembers "visitantes" visitante1
# Remover expiração de senha
samba-tool user setexpiry usuario1 --noexpiry 
samba-tool user setexpiry usuario2 --noexpiry
samba-tool user setexpiry visitante1 --noexpiry
# Altere a senha do Administrator para Abc1234.
samba-tool user setpassword Administrator
samba-tool user setexpiry Administrator --noexpiry
# Usuário para consulta LDAP através do módulo ldap
samba-tool user create ldap_user Abc1234. 
samba-tool user setexpiry ldap_user --noexpiry
```

## 2. Configuração do SERVIDOR DE AUTENTICAÇÃO

### Instalação e atualização do Debian 12

Referência: https://wiki.debian.org/SourcesList
```bash
nano /etc/apt/sources.list
```

```bash
deb http://deb.debian.org/debian bookworm main
deb-src http://deb.debian.org/debian bookworm main

deb http://deb.debian.org/debian-security/ bookworm-security main
deb-src http://deb.debian.org/debian-security/ bookworm-security main

deb http://deb.debian.org/debian bookworm-updates main
deb-src http://deb.debian.org/debian bookworm-updates main
```

```bash
apt update && apt upgrade
```

### Instalação dos pacotes

```bash
apt install -y samba winbind krb5-user acl chrony rsyslog freeradius freeradius-ldap 
apt-get install -y --no-install-recommends tzdata
ln -fs /usr/share/zoneinfo/America/Recife /etc/localtime
dpkg-reconfigure -f noninteractive tzdata
apt-get autoremove -y
apt-get clean
rm -rf /var/lib/apt/lists/*
```

### Configurações para ingresso no domínio `ad.lab.intranet`

Configuração de DNS

```bash
# Configuração do /etc/hosts. Comente uma linha e adicione a seguinte
#127.0.1.1     RADIUS01
192.168.15.4   RADIUS01.ad.lab.intranet RADIUS01
# Caso não possua servidor DNS
192.168.15.2 ADDC01.ad.lab.intranet ADDC01
192.168.15.3 ADDC02.ad.lab.intranet ADDC02
```

```bash
nano /etc/resolv.conf
```
```bash
search ad.lab.intranet lab.intranet
domain ad.lab.intranet
# Caso não possua servidor DNS use o do seu roteador Wi-Fi de casa
nameserver 192.168.15.1
# Caso possua servidores DNS
#nameserver 192.168.15.2
#nameserver 192.168.15.3
```

Configuração de HORA

```bash
nano /etc/chrony/chrony.conf
```
```bash
#pool 2.debian.pool.ntp.org iburst
server 192.168.15.2
server 192.168.15.3
```

```bash
systemctl enable chrony.service
systemctl start chrony.service
chronyc activity
chronyc sources
chronyc tracking
```

Configuração do Kerberos

```bash
nano /etc/krb5.conf
```
```bash
[libdefaults]
        default_realm = AD.LAB.INTRANET
        dns_lookup_realm = false
        dns_lookup_kdc = true
```

Configuração do Samba4

```bash
nano /etc/samba/smb.conf
```
```bash
[global]

        security = ADS
        workgroup = LAB
        realm = AD.LAB.INTRANET

        log file = /var/log/samba/%m.log
        log level = 1

        local master = no

        dedicated keytab file = /etc/krb5.keytab
        kerberos method = secrets and keytab
        winbind refresh tickets = yes

        #winbind trusted domains only = no (em desuso)
        winbind use default domain = yes
        winbind enum users  = yes
        winbind enum groups = yes

        # idmap config used for your domain.
        # Click on the following links for more information
        # on the available winbind idmap backends,
        # Choose the one that fits your requirements
        # then add the corresponding configuration.

        # Just adding the following three lines is not enough!!
        #  idmap config ad
        idmap config rid
        #  - idmap config autorid

        # Important: The ranges of the default (*) idmap config
        # and the domain(s) must not overlap!

        # Default idmap config used for BUILTIN and local accounts/groups
        idmap config *:backend = tdb
        idmap config *:range = 2000-9999

        # idmap config for domain
        idmap config LAB:backend = rid
        idmap config LAB:range = 10000-999999

        # Use template settings for login shell and home directory
        winbind nss info = template
        #template shell = /sbin/nologin
        template shell = /bin/bash
        template homedir = /home/%U
        #mkhomedir = yes
       #######################################
        #winbind use default domain = yes
        winbind cache time = 60
        winbind reconnect delay = 30
        #winbind enum users = yes
        #winbind enum groups = yes
        winbind expand groups = 10
        #template shell = /bin/bash
        #template homedir = /home/%U
        #######################################

```

```bash
samba-tool testparm
smbcontrol all reload-config
smbtree
/etc/init.d/winbind start
echo SAMBA_PASS | kinit SAMBA_USER
net ads join -U SAMBA_USER_ADMINISTRATOR%SAMBA_PASS
/etc/init.d/winbind restart
```

Testes de ingresso no domínio

```bash
kinit Administrator
klist
echo test | net ads testjoin
ntlm_auth --request-nt-key --domain=AD.LAB.INTRANET --username=Administrator --password=SAMBA_PASS
```

### Configuração do FreeRADIUS Version 3.2.1

## 2.1 Caso necessite realizar login em switches ou roteadores através de usuários incluídos em um grupo específico do Active Directory execute as configurações abaixo, se não vá para o item 2.2.

Crie um novo arquivo de política em `/etc/freeradius/3.0/policy.d/policy_ntlm_auth`
```
nano /etc/freeradius/3.0/policy.d/policy_ntlm_auth
```
```bash
# Os switches usam PAP e o Auth-Type PAP será comentado, então o Auth-Type não será definido. Isso faz com que caia nessa condicional o o Auth-Type seja atualizado para ntlm.
ntlm_auth.authorize {
    if (!control:Auth-Type && User-Password) {
        update control {
            Auth-Type := ntlm_auth
        }
    }
}
```

Adicione o tipo de autenticação ntlm_auth:

```
nano /etc/freeradius/3.0/sites-available/default
```
```bash
authorize {
    ...
    # pap
    ntlm_auth
}

authenticate {
        ...
#       Auth-Type PAP {
#               pap
#       }
        Auth-Type ntlm_auth {
                ntlm_auth
        }
}
```
### Configuração do módulo NTLM do winbind. Em `--require-membership-of` defina o grupo do Active Directory que poderá autenticar. Foi observado no wireshark que o password foi encriptado.
```
nano /etc/freeradius/3.0/mods-available/ntlm_auth
```
```bash
program = "/usr/local/samba/bin/ntlm_auth --request-nt-key --domain=%{%{mschap:NT-Domain}:-LAB} --require-membership-of='LAB\setor-a' --username=%{mschap:User-Name} --password=%{User-Password}"
```
Reinicie o serviço.
```
/etc/init.d/freeradius restart
```
Teste do módulo NTLM. Foi colocada uma __restrição de acesso__ no módulo para permitir somente usuários do grupo __setor-a__, portanto o usuário usuario2 deverá receber Acess-Reject.
```bash
#radtest username password servername port secret
radtest -t pap usuario1 Abc1234. 127.0.0.1 1812 testing123
radtest -t pap usuario2 Abc1234. 127.0.0.1 1812 testing123
```
## 2.2 Configuração do módulo MSCHAP para autenticação de usuários 802.1x.

Essa permissão para o usuário `freerad`, que foi criado ao instalar o pacote, no soquete do winbind do samba4.
```bash
setfacl -m u:freerad:rx /var/lib/samba/winbindd_privileged
/etc/init.d/winbind restart
```

Configure o módulo MS-CHAP para utilizar o NT/LM do winbind para checar usuário e senha no AD. O parâmetro `--require-membership-of` checa se o usuário é membro de um grupo.
```bash
nano /etc/freeradius/3.0/mods-available/mschap
```
```bash
ntlm_auth = "/usr/local/samba/bin/ntlm_auth --allow-mschapv2 --request-nt-key --domain=%{%{mschap:NT-Domain}:-LAB} --require-membership-of='LAB\domain users' --username=%{%{Stripped-User-Name}:-%{%{User-Name}:-None}} --challenge=%{%{mschap:Challenge}:-00} --nt-response=%{%{mschap:NT-Response}:-00}"
```

Altere a configuração do módulo eap de `md5` para `peap`. 
```bash
nano /etc/freeradius/3.0/mods-available/eap
```
```bash
eap {
    default_eap_type = peap
....
}

tls-config tls-common {
    ...
    random_file = /dev/urandom
    ...
}
```

Coloque um usuário no banco local em `/etc/freeradius/3.0/users`, para uma emergência caso o ADDC fique indisponível
```bash
setor-a Cleartext-Password := "Abc1234.", MS-CHAP-Use-NTLM-Auth := 0
        Tunnel-Type = "VLAN",
        Tunnel-Medium-Type = "IEEE-802",
        Tunnel-Private-Group-Id = 2

setor-a Cleartext-Password := "Abc1234.", MS-CHAP-Use-NTLM-Auth := 0
        Tunnel-Type = "VLAN",
        Tunnel-Medium-Type = "IEEE-802",
        Tunnel-Private-Group-Id = 3

DEFAULT Auth-Type == "MS-CHAP"
```

Reinicie o serviço.
```bash
/etc/init.d/freeradius restart
```

Teste MSCHAP

```bash
#Teste com método de autenticação MS-CHAP
#radtest username password servername port secret
radtest -t mschap usuario1 Abc1234. 127.0.0.1:1812 0 testing123
radtest -t mschap usuario2 Abc1234. 127.0.0.1:1812 0 testing123
radtest -t mschap setor-a Abc1234. 127.0.0.1:1812 0 testing123
radtest -t mschap setor-b Abc1234. 127.0.0.1:1812 0 testing123
```
## 2.3 Configuração do LDAP para limitar acesso e definir vlan's por grupos do Active Directory.

Caso ainda não tenha instalado, instal o pacote:
```bash
apt install -y freeradius-ldap
```

Descomente o tipo de autenticação LDAP :

```bash
nano /etc/freeradius/3.0/sites-available/default
```
```bash
authorize {
    ...
    -ldap
}
```

Edite a configuração do módulo. Será necessário criar um usuário no Active Directory com privilégios mínimos. Criei o `ldap_search` . Ele buscará no AD os grupos do usuário. Esses grupos serão necessários para atender as condições impostas no arquivo `/etc/freeradius/3.0/users`:

```bash
ldap {
  server = 'ldaps://ADDC01'
  server = 'ldaps://ADDC02'
  port = 636
	identity = 'ldap_search@ad.lab.intranet'
	password = Abc1234.
	base_dn = 'dc=ad,dc=lab,dc=intranet'
	sasl {
	}
	update {
		control:Password-With-Header	+= 'userPassword'
		control:NT-Password		:= 'ntPassword'
		reply:Reply-Message		:= 'radiusReplyMessage'
		reply:Tunnel-Type		:= 'radiusTunnelType'
		reply:Tunnel-Medium-Type	:= 'radiusTunnelMediumType'
		reply:Tunnel-Private-Group-ID	:= 'radiusTunnelPrivategroupId'
		control:			+= 'radiusControlAttribute'
		request:			+= 'radiusRequestAttribute'
		reply:				+= 'radiusReplyAttribute'
	}
	edir = no
	user_dn = "LDAP-UserDn"
	user {
		base_dn = "${..base_dn}"
		filter = "(samaccountname=%{%{Stripped-User-Name}:-%{User-Name}})"
		sasl {
		}
	}
	group {
		base_dn = "${..base_dn}"
		filter = '(objectClass=group)'
		name_attribute = cn
    membership_attribute = 'memberOf'
	}
	profile {
	}
	accounting {
		reference = "%{tolower:type.%{Acct-Status-Type}}"
		type {
			start {
				update {
					description := "Online at %S"
				}
			}
			interim-update {
				update {
					description := "Last seen at %S"
				}
			}
			stop {
				update {
					description := "Offline at %S"
				}
			}
		}
	}
	post-auth {
		update {
			description := "Authenticated at %S"
		}
	}
	options {
		chase_referrals = yes
		rebind = yes
		res_timeout = 10
		srv_timelimit = 3
		net_timeout = 1
		idle = 60
		probes = 3
		interval = 3
		ldap_debug = 0x0028
	}
	tls {
		require_cert	= 'never'
	}
	pool {
		start = ${thread[pool].start_servers}
		min = ${thread[pool].min_spare_servers}
		max = ${thread[pool].max_servers}
		spare = ${thread[pool].max_spare_servers}
		uses = 0
		retry_delay = 30
		lifetime = 0
		idle_timeout = 60
	}
}
```

Habilitar módulo LDAP

```bash
ln -s /etc/freeradius/3.0/mods-available/ldap /etc/freeradius/3.0/mods-enabled/
chown -h freerad:freerad /etc/freeradius/3.0/mods-enabled/ldap
```

Inicie o serviço no modo DEBUG para checagem da conexão do módulo ldap com o Active Directory

```bash
freeradius -X
```

Modifique o banco local `/etc/freeradius/3.0/users`, para checar se o usuário tem o grupo e defina a vlan do grupo

```bash
# Usuário para autenticação EAP-TLS
impressora Cleartext-Password := "Abc1234.", MS-CHAP-Use-NTLM-Auth := 0
        Tunnel-Type = "VLAN",
        Tunnel-Medium-Type = "IEEE-802",
        Tunnel-Private-Group-Id = 2

# Usuário para Linux
setor-a Cleartext-Password := "Abc1234.", MS-CHAP-Use-NTLM-Auth := 0
        Tunnel-Type = "VLAN",
        Tunnel-Medium-Type = "IEEE-802",
        Tunnel-Private-Group-Id = 2

# Usuário para Linux
setor-a Cleartext-Password := "Abc1234.", MS-CHAP-Use-NTLM-Auth := 0
        Tunnel-Type = "VLAN",
        Tunnel-Medium-Type = "IEEE-802",
        Tunnel-Private-Group-Id = 3

# Usuário para Windows e admin de rede autenticar em switches
DEFAULT LDAP-Group == "setor-a"
        Cisco-AVPair = "shell:priv-lvl=15",
        Tunnel-Type = "VLAN",
        Tunnel-Medium-Type = "IEEE-802",
        Tunnel-Private-Group-Id = 2

# Usuário para Windows
DEFAULT LDAP-Group == "setor-b"
        Tunnel-Type = "VLAN",
        Tunnel-Medium-Type = "IEEE-802",
        Tunnel-Private-Group-Id = 3

# Restrição para usuários do grupo visitante, com solicitações originadas do switch 192.168.3.1 e MAC do dispositivo
DEFAULT LDAP-Group == "visitantes", NAS-IP-Address = 192.168.3.1, Calling-Station-Id = "0800.27eb.0978"
        Tunnel-Type = "VLAN",
        Tunnel-Medium-Type = "IEEE-802",
        Tunnel-Private-Group-Id = 5

# Rejeita todos que não estão nos grupos abaixo
DEFAULT LDAP-Group != "setor-a", Auth-Type := Reject
        Reply-Message = "Nao pertence aos grupos autorizados"
DEFAULT LDAP-Group != "setor-b", Auth-Type := Reject
        Reply-Message = "Nao pertence aos grupos autorizados"
DEFAULT LDAP-Group != "visitantes", Auth-Type := Reject
        Reply-Message = "Nao pertence aos grupos autorizados"
```

Reinicie o serviço e teste as novas configurações. Observe se a resposta de cada um corresponde as vlans definidas no arquivo users.

```bash
radtest -t mschap usuario1 Abc1234. 127.0.0.1:1812 0 testing123
radtest -t mschap usuario2 Abc1234. 127.0.0.1:1812 0 testing123
radtest -t mschap setor-a Abc1234. 127.0.0.1:1812 0 testing123
radtest -t mschap setor-b Abc1234. 127.0.0.1:1812 0 testing123
# Esses usuário deverão ser rejeitados por não pertencerem aos grupos setor-a e setor-b
radtest -t mschap Administrator Abc1234. 127.0.0.1:1812 0 testing123
radtest -t mschap ldap_search Abc1234. 127.0.0.1:1812 0 testing123
```
#### __Criação de cadeia de certificados para serem usados pelos Suplicantes e Servidores Radius__

Configure editando os arquivos `.cnf` em `/etc/freeradius/3.0/certs/`

CA.CNF
```bash
[ CA_default ]
...
default_days            = 9131
default_crl_days        = 9100
...
crlDistributionPoints   = URI:http://www.lab.intranet/lab_ca.crl
...
[ req ]
...
input_password          = Abc1234.
output_password         = Abc1234.
...
[certificate_authority]
countryName             = BR
stateOrProvinceName     = PE
localityName            = Recife
organizationName        = LAB
emailAddress            = admin@lab.intranet
commonName              = "LAB Radius Certificate Authority"
[v3_ca]
...
crlDistributionPoints   = URI:http://www.lab.intranet/lab_ca.crl
```
SERVER.CNF
```bash
[ CA_default ]
...
default_days            = 9131
default_crl_days        = 9100
...
[ req ]
...
input_password          = Abc1234.
output_password         = Abc1234.

[server]
countryName             = BR
stateOrProvinceName     = PE
localityName            = Recife
organizationName        = LAB
emailAddress            = admin@lab.intranet
commonName              = "LAB Radius Server Certificate"
```
CLIENTS.CNF
```bash
[ CA_default ]
...
default_days            = 9131
default_crl_days        = 9100
...
[ req ]
...
input_password          = Abc1234.
output_password         = Abc1234.

[client]
countryName             = BR
stateOrProvinceName     = PE
localityName            = Recife
organizationName        = LAB
emailAddress            = impressora1@lab.intranet
commonName              = impressora1@lab.intranet
```

Geração dos certificados. Em `/etc/freeradius/3.0/certs` execute:
```bash
make ca
make server
```
Caso seja necessário criar um certificado único para um dispositivo que irá utilizar EAP-TLS, por exemplo, impressoras de empresas contratadas para serviço de impressão, serão necessários os certificado ca.pem e impressora1@lab.intranet.pem para a configuração. Caso precise de certificados para novos dispositivos, altere os campos emailAddress e commonName em client.cnf e execute:
```bash
make client
```

Destruição de certificados, caso precise remover para geração de novos.
```bash
rm -f *~ dh *.csr *.crt *.p12 *.der *.pem *.key index.txt* serial*  *\.0 *\.1 ca-crl.pem ca.crl
```

Configure o módulo EAP com a localização dos novos certificados, pois até agora ele estava usando os certificados padrões do S.O. e o Suplicante teria que ter o certificado descrito na linha ca_file para a comunicação ter sucesso.
```bash
nano /etc/freeradius/3.0/mods-available/eap
```

```bash
        tls-config tls-common {
                # Senha utilizada nos arquivos *.cnf nos campos input_password e output_password
                private_key_password = Abc1234.
                private_key_file = ${certdir}/server.key
                ...
                certificate_file = ${certdir}/server.pem
                ...
                ca_file = ${certdir}/ca.pem
                ...
                # Foi necessário para funcionar em outras distribuições Linux diferente do servidor
                disable_tlsv1_2 = no
                disable_tlsv1_1 = no
                disable_tlsv1 = no
                ...
                # Foi necessário para funcionar em outras distribuições Linux diferente do servidor
                tls_min_version = "1.0"
                tls_max_version = "1.2"
                ...
```

Permita o usuário freeradius ler os certificados server.key e server.pem
```bash
chmod 644 server.pem server.key
chown freerad.freerad server.key server.pem
```

Copie o certificado `ca.pem` para instalar nos suplicantes Linux. Colocando em `/etc/ssl/certs/` o suplicante não precisa indicar o local do CA em suas configurações 802.1x.
```bash
scp ca.pem admlocal@192.168.15.XX:/etc/ssl/certs/
```

Os certificados gerados com o prefixo `client.*` e `email_do_client.pem` serão usados em suplicantes que usarão autenticação EAP-TLS ou PEAP com EAP-TLS. Será necessário inserir no arquivo `users` do servidor somente o nome do cliente que foi inserido no campo identidade da configuração 802.1x do suplicante.

Gere um certificado para cada suplicante se desejar, alterando os dados do arquivo `client.cnf` nos campos `emailAddress, commonName, default_days, default_crl_days, input_password e output_password` em seguida execute o comando `make client`.

Na configuração 802.1x do suplicante serão utilizados o `ca.pem`, qualquer um dos seguintes certificados `client.crt, client.der, client.pem, client.p12, client.key, email_do_client.pem` e a senha inserida em `client.cnf`. Recomendo utilizar o certificado `email_do_client.pem` pois os outros podem ser sobrescritos.

Referência: http://deployingradius.com/documents/configuration/certificates.html


__DEBUG__
```bash
/etc/init.d/freeradius stop
freeradius -X
```

Permita o Radius Server aceitar conexões do range dos clientes também chamados de NAS ou authenticator, que são os dispositivos de rede como roteadores e switches com função de autenticador de suplicante.
```bash
nano /etc/freeradius/3.0/clients.conf
```
```bash
cliente localhost {
    ...
        ipaddr = * # Aceita qualquer IP.
        #ipaddr = 192.168.15.0/24
        secret = Abc1234.
    ...
}
```
O Windows envia o usuário junto com o domínio(DOMAIN\username). Precisamos descartar o domínio para que consulta no AD ocorra com sucesso. Modifique os aquivos abaixo:

```bash
nano /etc/freeradius/3.0/proxy.conf
```
```bash
# Nesse arquivo deverá ter somente estas linhas
realm LAB{
}
```
```bash
nano /etc/freeradius/3.0/sites-available/default
```
```bash
authorize {
    ...
    ntdomain
    ...
}
preacct {
    ...
    ntdomain
    ...
}
```
```bash
nano /etc/freeradius/3.0/sites-available/inner-tunnel
```
```bash
authorize {
    ...
    ntdomain
    ...
}
```

## 3. Teste de desempenho

A partir de um host ou vários, execute em vários terminais:
```bash
echo "User-Name=USUARIO,User-Password=SENHA" radclient IP_SERVIDOR_RADIUS:1812 auth SENHA_CLIENTE -s -c 100
```

## 4. Refinamento da configurações para implantação do servidor RADIUS em Produção

Os arquivos de configuração estão na pasta /etc. Segue a lista da localização dos arquivos a serem substituídos ou modificados.

```bash
/etc/freeradius/3.0/users
/etc/freeradius/3.0/mods-available/eap
/etc/freeradius/3.0/clients.conf
/etc/freeradius/3.0/sites-available/default
/etc/freeradius/3.0/sites-available/inner-tunnel
/etc/freeradius/3.0/radiusd.conf
/etc/freeradius/3.0/mods-available/realm
# Módulo para autenticação em Switches com credenciais do AD
/etc/freeradius/3.0/mods-available/ntlm_auth
/etc/freeradius/3.0/policy.d/policy_ntlm_auth
# Módulo para 802.1x com credenciais do AD
/etc/freeradius/3.0/mods-available/mschap
/etc/freeradius/3.0/proxy.conf
/etc/freeradius/3.0/mods-available/linelog
```

Desabilitando módulos sem utilização

```bash
rm /etc/freeradius/3.0/mods-enabled/chap
rm /etc/freeradius/3.0/mods-enabled/digest
rm /etc/freeradius/3.0/mods-enabled/exec
rm /etc/freeradius/3.0/mods-enabled/soh
rm /etc/freeradius/3.0/mods-enabled/pap
```