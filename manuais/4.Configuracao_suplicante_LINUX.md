## Configuração do SUPLICANTE LINUX

Copie o certificado `ca.pem` do servidor Radius para `/etc/ssl/certs` e renomeie para `8021x.pem`. A utilização do certificado obriga o suplicante verificar o certificado do servidor, permitindo validar a identidade do servidor legítimo.

Opção 1. Modo gráfico
1. Encontre a aba Segurança nas configurações da placa de rede.
* Autenticação: `EAP protegido (PEAP)`
* Identidade anônima: `em branco ou nome do usuário ou nome qualquer`
* Domínio: `Nunca preencher se esse campo aparecer`
* Certificado CA: Localize o certificado em `/etc/ssl/certs`
* Versão PEAP: `Automático`
* Autenticação interna: `MSCHAPv2`
* Insira o usuário e senha da VLAN correspondente
**<div align="left"></div>**
![](../imagens/config_linux/1.png)

Opção 2. Via terminal
```bash
# Confirme se o nome da conexão é "Conexão cabeada 1"
sudo nmcli connection show --active
# Execute a modificação da conexão
sudo nmcli con mod "Conexão cabeada 1" \
  802-1x.ca-cert "/etc/ssl/certs/8021x.pem" \
  802-1x.eap "peap" \
  802-1x.identity "setor-a" \
  802-1x.password "Abc1234." \
  802-1x.phase2-auth "mschapv2" \
  ipv6.method "ignore"
```
